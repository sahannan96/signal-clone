// import * as firebase from 'firebase'
// import "firebase/firestore"
// import "firebase/auth"

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDYf_Mfh_ngLYbFxa0uBZ2PcDcpJrcw8rs",
    authDomain: "signal-clone-7a06c.firebaseapp.com",
    projectId: "signal-clone-7a06c",
    storageBucket: "signal-clone-7a06c.appspot.com",
    messagingSenderId: "297492288793",
    appId: "1:297492288793:web:e13dee10163fc11ec8e188",
    measurementId: "G-RTJ9ZN2W01"
};

let app
if (firebase.apps.length === 0) {
    app = firebase.initializeApp(firebaseConfig)
} else {
    app = firebase.app()
}

const db = app.firestore()
const auth = firebase.auth()
export { db, auth }